import React from 'react';

const UserContext = React.createContext()

// context provider component allows context to change subscriptions of child components

export const UserProvider = UserContext.Provider

export default UserContext;