import { useContext } from 'react'
import { Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import styled from 'styled-components';
import UserContext from '../contexts/UserContext';


const Styles = styled.div`

.navibar {
    background-color: #424B54;
}

.dropitem {
    color: black !important; 
}

${'' /* .navlink {
    color: rgba(255,255,255,.5);
    text-decoration: none;
    padding-right: .5rem;
    padding-left: .5rem;
    padding: .5rem 1rem;
    font-size: medium;
    opacity: .9;
    display: block;
}

.navdrop {
    color: rgba(255,255,255,.5);
    text-decoration: none;
    font-size: medium;
    text-decoration: none;
}

.navlink:hover {
    border: solid;
    border-radius: 10px;
} */}

`

function navLoggedOut() {
    return (
        <Nav className='ml-auto'>
                <Link href='/' passHref>
                    <Nav.Link>Login</Nav.Link>
                </Link>
                <Link href='/register' passHref>
                    <Nav.Link>Register</Nav.Link>
                </Link>
        </Nav>
    )
}

function navLoggedIn() {
    return (
        <>
        <Nav className='mr-auto'>

                    <Link 
                        href='/user/categories' passHref>
                        <Nav.Link>Categories</Nav.Link>
                    </Link>
                    <Link href='/user/records' passHref>
                        <Nav.Link>Records</Nav.Link>
                    </Link>

                    <Link href='/user/charts/category-breakdown' passHref>
                        <Nav.Link>
                            Category Breakdown
                        </Nav.Link>        
                    </Link>

        </Nav>
        
        <Nav className='ml-auto'>
            <Link href='/logout' passHref>
                <Nav.Link>Logout</Nav.Link>
            </Link>
        </Nav>
        </>
    )
}

export default function NavBar() {
    
    let isLoggedin = false
    const { user } = useContext(UserContext)

    if(user.email !== null) {
        isLoggedin = true
       }

    return (
    <>
    <Styles>
        <Navbar className='navibar' variant='dark' expand='lg' fixed='top'>
            <Link href='/' passHref className='navbar-brand'>
                <Navbar.Brand>Sabby!</Navbar.Brand>
            </Link>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse id='basic-navbar-nav'>
                   { isLoggedin ? navLoggedIn() : navLoggedOut() }
            </Navbar.Collapse>
        </Navbar>
        </Styles>
    </>
    )
}
