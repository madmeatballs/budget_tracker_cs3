import GoogleLogin from 'react-google-login';
import AppHelper from '../apphelper';


export default function GloginBtn() {
    
    const resGoogle = (res) => {
        console.log(res.tokenId)

        const payload = {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({ tokenId: res.tokenId })
        }
        fetch(`${AppHelper.API_BASE_URI}/users/verify-google-id-token`, payload)
        .then(res => res.json())
        .then(data => {
            console.log(data)
        })
    }
    
    return (
        <GoogleLogin
            clientId='218659367967-3c70ouhm8he365ab1j3htqlbpngtrjs1.apps.googleusercontent.com'
            onSuccess={resGoogle}
            onFailure={resGoogle}
            cookiePolicy={'single_host_origin'}
            buttonText='Login with Google'
            theme='light'
        />
    )
}
