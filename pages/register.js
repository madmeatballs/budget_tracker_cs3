import {useState, useEffect} from 'react'
import {Form,Button} from 'react-bootstrap'
import Router from 'next/router'
import { Layout } from '../components/Layout'
import AppHelper from '../apphelper'

//import Swal
import Swal from 'sweetalert2'
import styled from 'styled-components'
import View from '../components/View'
// import GloginBtn from '../components/GloginBtn'

const Styles = styled.div`
	.reg-form {
		background-color: white;
		border-radius: 10px;
		padding: 30px;
		margin: auto;
	}

	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
	-webkit-appearance: none; 
	margin: 0; 
	}

	.orDiv {
		line-height: -.5;
		text-align: center;

		span {
		display: inline-block;
		position: relative;
		}

		span:before, span:after {
			content: '';
			position: absolute;
			height: 12px;
			border-bottom: 2px solid lightgray;
			top: 0;
			width: 200px;
		}

		span:before {
			right: 100%;
			margin-right: 15px;
		}

		span:after {
			left: 100%;
			margin-left: 15px;
		}

	}



`

export default function Register(){
    /*What do we bind to our input to track user input in real time?*/
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState('')
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	//stare for conditional rendering for the submit button
	const [ isActive, setIsActive ] = useState(true)

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	//lets now create our register method
	function registerUser(e) {
		e.preventDefault() //to avoid page redirection.
		//lets check if an email exist in our records. 
		fetch(`${AppHelper.API_BASE_URI}/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			}) //convert the value to string data type to be accepted in the API
		}).then(res => res.json()).then(data => {
			//lets create a checker just to see if may nakukuha tayo na data
			console.log(data)
			//lets create a control structure to give the appropriate response according to the value of the data.
			if(data === false){
				//if the return in false then allow the user to register otherwise NOPE
				fetch(`${AppHelper.API_BASE_URI}/api/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				}).then(AppHelper.toJSON).then(data => {
					//you can create a checker here if you wish
					console.log(data)
					if(data) {
						Swal.fire({
							icon: 'success',
							title: 'Successfully Registered',
							text: 'Thank you for registering.'
						})
						//after displaying a success message redirect the user to the login page
						Router.push('/')
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Registration failed',
							text: 'Something went wrong'
						})
					}
				})
			} else {
				//the else branch will run if there return value is true
				Swal.fire({
					icon: 'error',
					title: 'Registration Failed',
					text: 'Email is already taken by someone else.'
				})
			}
		})
	}

   return (
		<>
		<Layout>
			<Styles>
			<View title='Register'>
				<div className='reg-form'>
				<h1 className="mt-3 text-center">Register</h1>
				<Form onSubmit={e => registerUser(e)} className="mb-3">
					<Form.Group controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="userLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="mobileNo">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
					</Form.Group>

					{
						isActive ? <Button variant="dark" type="submit">Register</Button> : 
						<Button variant="danger" disabled>Register</Button>
					}


					{/* <div className='text-muted orDiv pt-3'><span>Or</span></div>

					<div className='text-center pt-3'><GloginBtn /></div> */}

				</Form>
				</div>
				</View>
				</Styles>
			</Layout>
		</>
		) 
}