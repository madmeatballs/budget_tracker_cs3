import { useState, useContext } from 'react';
import { Form, Button, Jumbotron } from 'react-bootstrap';
import UserContext from '../contexts/UserContext';
import AppHelper from '../apphelper'
import View from '../components/View';
import { Layout } from '../components/Layout';
import Swal from 'sweetalert2';
import Router from 'next/router';
import styled from 'styled-components'
// import GloginBtn from '../components/GloginBtn';

const Styles = styled.div`

.login-form {
	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;
}

.orDiv {
		line-height: -.5;
		text-align: center;

		span {
		display: inline-block;
		position: relative;
		}

		span:before, span:after {
			content: '';
			position: absolute;
			height: 12px;
			border-bottom: 2px solid lightgray;
			top: 0;
			width: 200px;
		}

		span:before {
			right: 100%;
			margin-right: 15px;
		}

		span:after {
			left: 100%;
			margin-left: 15px;
		}

	}

`

export default function Home() {
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const { user, setUser } = useContext(UserContext)
   
	  if(user.email !== null) {
	   Router.push('/user/records');
	  }
	  
	 //lets create a function to retrieve the user details before the client can be redirected inside the rECORDS page. 
	 const retrieveUserDetails = (accessToken) => {
		//we have to make sure the user has already been verified meaning it was already granted access token.
		const options = {
			headers: { Authorization: `Bearer ${accessToken}`}
		}
		//create a request going to the desired enspoint with the payload.
		fetch(`${AppHelper.API_BASE_URI}/api/users/details`, options).then((response) => response.json()).then(data => {
			setUser({ id: data._id }) 
			Router.push('/user/records') 
		})
	 }
   
	 function login(e){
	   e.preventDefault()
	   //describe the request to login
	   fetch(`${AppHelper.API_BASE_URI}/api/users/login`, {
		   method: 'POST',
		   headers: {
			   'Content-Type': 'application/json'
		   },
		   body: JSON.stringify({
			   email: email,
			   password: password
		   }) //for the request to be accepted by the api
	   }).then(res => res.json()).then(data => {
		   //lets create a control structure
		   if(typeof data.accessToken !== "undefined") {
			   //store the access token in the local storage
			   localStorage.setItem('token', data.accessToken)
			   retrieveUserDetails(data.accessToken)
			   Swal.fire({
				   icon: 'success',
				   title: 'Successfully Logged In'
			   })
			
		   } else {
			  Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
		   }
	   })
	 }
   

  return (
    <>
	<Layout>
      <View title={`Sabby! - Budget Assistant`}>
	  <Styles>
        <Jumbotron>
			<h1>Hello!</h1>
			<p>It's Sabby, your own personal budget assistant!</p>
		</Jumbotron>
	        <div className='login-form'>
				<Form onSubmit={e => login(e)}>
					<Form.Group controlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="text" value={email} onChange={e=>setEmail(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" value={password} onChange={e=>setPassword(e.target.value)} required/>
					</Form.Group>
					<Button type="submit" variant="dark" className="btn-block mb-3">Submit</Button>
					<p className='text-muted text-center'>
						Not registered yet? <a href='/register' className='font-italic'>Sign up here!</a>
					</p>
					{/* <div className='text-muted orDiv'><span>Or</span></div>
					<div className='text-center pt-3'><GloginBtn /></div> */}
				</Form>
			</div>
		</Styles>
      </View>
	  </Layout>
    </>
  )
}
