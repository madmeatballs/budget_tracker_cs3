import { useEffect, Fragment, useContext } from 'react'
import Router from 'next/router'
import View from '../../components/View'
import UserContext from '../../contexts/UserContext'
import Spinner from 'react-bootstrap/Spinner'

export default function logout(){
  
    const { unsetUser } = useContext(UserContext)

    useEffect(() => {
        unsetUser()
        Router.push('/')
    }, []);

    return (
            <View title="Logout">
                <Spinner animation="border" role="status">
                    <span className="sr-only text-center">Logging out...</span>
                </Spinner>
                <h6 className="text-center">You will be redirected back to the login page shortly.</h6>   
            </View>
    )
}
