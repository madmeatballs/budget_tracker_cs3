import { useState, useEffect } from 'react'
import { Form, Col } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import View from '../../../components/View'
import AppHelper from '../../../apphelper'
import { colorRandomizer } from '../../../colorRandomizer'
import moment from 'moment'
import styled from 'styled-components'

const Styles = styled.div`

.cat-break {
    background-color: white;
    border-radius: 10px;
    padding: 30px;
    margin: auto;
}

`

export default function CategoryBreakdown(){
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: bgColors
            }
        ]
    };

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`${ AppHelper.API_BASE_URI }/api/users/get-records-breakdown-by-range`, payload)
            .then(AppHelper.toJSON)
            .then(data => {
                setLabelsArr(data.map(record => record.categoryName))
                setDataArr(data.map(record => record.totalAmount))
                setBgColors(data.map(()=> `#${colorRandomizer()}`))
        })

    }, [fromDate, toDate])


    return (
        <Styles>
            <View title="Category Breakdown">
            <div className='cat-break'>
                <h3>Category Breakdown</h3>
                <Form.Row>
                    <Form.Group as={ Col } xs="6">
                        <Form.Label>From</Form.Label>
                        <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                    </Form.Group>
                    <Form.Group as={ Col } xs="6">
                        <Form.Label>To</Form.Label>
                        <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                    </Form.Group>
                </Form.Row>
                <hr/>
                <Pie 
                    height={80}
                    width={80}
                    options={{ maintainAspectRatio: false }}
                    data={data}
                />
            </div>
            </View>
        </Styles>
    )
}

