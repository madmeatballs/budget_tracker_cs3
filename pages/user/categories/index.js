import { useState, useEffect } from 'react'
import { Table, Button, Form } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../apphelper'
import styled from 'styled-components'
import Swal from 'sweetalert2'

const Styles = styled.div`

.cat-table {
    background-color: white;
    border-radius: 10px;
    padding: 30px;
    margin: auto;
}

`

const Categories = () => {
   
    const [categories, setCategories] = useState([]);
    const [ random, setRandom ] = useState('');
    const [ random1, setRandom1 ] = useState('');
    const [ random2, setRandom2 ] = useState('');

    useEffect(() => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        }
        
        fetch(`${AppHelper.API_BASE_URI}/api/users/user-categories`, payload)
            .then(AppHelper.toJSON)
            .then(data => {
                console.log(data)

                data.categories.map(category => {
                    console.log(category)
                    setCategories(category)
                })
            })
    }, [])

    function createTable() {
        return (
            categories.map((item) => {
            return (
                <tr key={item._id}>
                    <td>{item.name}</td>
                    <td>{item.type}</td>
                </tr>
            )})
        )
    }

    const hClick = (e) => {
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                random: random,
                random1: random1,
                random2: random2,
            })
        }
        
        fetch(`${AppHelper.API_BASE_URI}/api/random/`, payload)
            .then(AppHelper.toJSON)
            .then(data => 
                {
                    console.log('hello')
                    if(data){
                        Swal.fire({
                            icon: "success",
                            title: "Successfully added random",
                            text: "Thank you!"
                        })
                    }else{
                        Swal.fire({
                            icon: "error",
                            title: "Failed to random",
                            text: "Something went wrong"
                        })
            
                    }
                })
    }    

    return (
        <Styles>
            <View title="Categories">
            <div className='cat-table'>
                <h3>Categories</h3>
                <Link href="/user/categories/new"><a className="btn btn-info mt-1 mb-3">Add</a></Link>
                <Form onSubmit={e => hClick(e)}>
                <Form.Group className="mb-3" controlId="formBasicRandom">
                    <Form.Label>Random</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="enter text"
                        value={ random } 
                        onChange={ (e) => setRandom(e.target.value) }
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicRandom">
                    <Form.Label>Random1</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="enter text"
                        value={ random1 } 
                        onChange={ (e) => setRandom1(e.target.value) }
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicRandom">
                    <Form.Label>Random2</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="enter text"
                        value={ random2 } 
                        onChange={ (e) => setRandom2(e.target.value) }
                    />
                </Form.Group>
                    <Button type='submit'>Test</Button>
                </Form>
                
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody> 
                        {createTable()}
                    </tbody>
                </Table>
            </div>
            </View>
        </Styles>
    )
}


export default Categories;