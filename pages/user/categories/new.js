import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View'
import AppHelper from '../../../apphelper'

const AddCategory = () => {
    return (
        <View title="New Category">
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>New Category</h3>
                    <Card>
                        <Card.Header>Category Information</Card.Header>
                        <Card.Body>
                            <NewCategoryForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewCategoryForm = () => {
    const [categoryName, setCategoryName] = useState('')
    const [typeName, setTypeName] = useState(undefined)


const createCategory = (e) => {
    e.preventDefault()

    const payload = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${AppHelper.getAccessToken()}`
        },
        body: JSON.stringify({
            name: categoryName,
            typeName: typeName
        })
}

fetch(`${AppHelper.API_BASE_URI}/api/users/add-category`, payload)
    .then(AppHelper.toJSON)
    .then(data => {
        if(data == true){
            Swal.fire({
                icon: "success",
                title: "Successfully added the Record",
                text: "Thank you!"
            })
        }else{
            Swal.fire({
                icon: "error",
                title: "Failed to add Record",
                text: "Something went wrong"
            })

        }
    })
}


    return (
        <Form onSubmit={ (e) => createCategory(e) }>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter category name" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={ typeName } onChange={ (e) => setTypeName(e.target.value) } required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Button variant="primary" type="submit">Submit</Button>
        </Form>
    )
}


export default AddCategory;