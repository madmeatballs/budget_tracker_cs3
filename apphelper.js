module.exports = {
    API_BASE_URI: process.env.NEXT_PUBLIC_API_BASE_URI,
    getAccessToken: () => localStorage.getItem('token'),
    toJSON: res => res.json()
}